using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.Collections.Generic;

public class Bot {
	public static void Main(string[] args) {
		const string TRACK_KEY = "TRACK_NAME";
	    string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];
		string track = null;
		if (System.Environment.GetEnvironmentVariables().Contains(TRACK_KEY))
			track = (string)System.Environment.GetEnvironmentVariables()[TRACK_KEY];

		Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey + " : " + track);

		using(TcpClient client = new TcpClient(host, port)) {
			NetworkStream stream = client.GetStream();
			StreamReader reader = new StreamReader(stream);
			StreamWriter writer = new StreamWriter(stream);
			writer.AutoFlush = true;

			new Bot(reader, writer, new Join(botName, botKey), track != null ? new JoinRace(botName, botKey, track) : null);
		}
	}

	//const float ANGLE = 0.485f;
	//const float DMP = 0.985f;

	const float ANGLE = 0.47f;
	const float DMP = 0.975f;
	private StreamWriter writer;
	GameInit init;
	float lastInPieceDistance;
	float totalDistance;
	int lastIndex;
	int c = 0;
	YourCar car;
	int bestLane = 0;
	bool switchSent = false;
	bool switching = false;
	bool gameStarted = false;
	Dictionary<int,int> cars = new  Dictionary<int,int>();

	void UpdateCarPositions(CarPositions poses)
	{
		for (int i = 0; i < poses.data.Length; i++)
		{
			cars[i] = poses.data[i].piecePosition.pieceIndex;
		}
	}

	int GetCarIndex(CarPositions poses)
	{
		for (int i = 0; i < poses.data.Length; i++)
			if (poses.data[i].id.name.Equals(car.data.name))
				return i;

		System.Console.WriteLine("I'm COLOR BLIND!!!!");
		return 0;
	}

	float EstimateiBreakDistance(float currSpeed, float finalSpeed)
	{
		return (finalSpeed - currSpeed) / (float)Math.Log(DMP);
	}

	float EstimateMaxSpeedToPiece(float finalSpeed, float distance)
	{
		return -distance * (float)Math.Log(DMP) + finalSpeed;
	}

	float EstimatePieceLength(int pieceIdx, int laneIdx)
	{
		return init.data.race.track.pieces[pieceIdx].length + GetRadius(pieceIdx, laneIdx) * (float)Math.PI * (float)Math.Abs(init.data.race.track.pieces[pieceIdx].angle) / 180.0f;
	}

	float GetRadius(int pieceIdx, int laneIdx)
	{
		float res = init.data.race.track.pieces[pieceIdx].radius;
		if (res >  0.0f)
			res -= init.data.race.track.lanes[laneIdx].distanceFromCenter * Math.Sign(init.data.race.track.pieces[pieceIdx].angle);

		return res;
	}

	int GetCurrentLane(int carIdx, CarPositions poses)
	{
		return poses.data[carIdx].piecePosition.lane.startLaneIndex;
	}

	int GetNextLane(int carIdx, CarPositions poses)
	{
		return poses.data[carIdx].piecePosition.lane.endLaneIndex;
	}

	float GetRadius(int carIdx, int pieceIdx, CarPositions poses)
	{
		float res = init.data.race.track.pieces[pieceIdx].radius;
		if (res >  0.0f)
			res -= init.data.race.track.lanes[GetCurrentLane(carIdx, poses)].distanceFromCenter * Math.Sign(init.data.race.track.pieces[pieceIdx].angle);

		return res;
	}

	int GetNextIndex(int curIdx)
	{
		curIdx++;
		curIdx = (curIdx + init.data.race.track.pieces.Length) % init.data.race.track.pieces.Length;
		return curIdx;
	}

	float GetMaxSpeed(float r)
	{
		r = (float)Math.Abs(r);
		if (r > 0f)
			return (float)Math.Min((float)Math.Sqrt(r*ANGLE), 10f);
		return 10f;
	}

	float GetMaxThrotle(float maxSpeed, float speed)
	{
		if (maxSpeed - speed < 0.2f)
			return (maxSpeed - speed) / 0.2f;
		return 1f;
	}

	void UpdateBestLane(int pieceIdx, CarPositions poses)
	{
		var currLane = GetCurrentLane(c, poses);
		var bestTotal = 1e9f;
		for (int i = 0; i < init.data.race.track.lanes.Length; i++)
		{
			var tmp = EstimateSegmentTime(pieceIdx, i, poses);
			if (i == currLane)
			{
				foreach (var key in cars.Keys)
				{
					System.Console.WriteLine("Car: {0}, lane {1}, piece {2}, distance {3}", key, GetCurrentLane(key, poses), poses.data[key].piecePosition.pieceIndex, poses.data[key].piecePosition.inPieceDistance);
					if (c != key && GetCurrentLane(key, poses) == currLane && 
						poses.data[c].piecePosition.pieceIndex == poses.data[key].piecePosition.pieceIndex && 
						poses.data[c].piecePosition.inPieceDistance < poses.data[key].piecePosition.inPieceDistance)
						{
							System.Console.WriteLine(">>>Found a car{0} on the same lane{1}!", key, currLane);
							tmp *= 1.15f;
						}
				}
		//			if (cars[key] 
			}
			System.Console.WriteLine("Lane {0} estimate: {1}", i, tmp);
			if (tmp < bestTotal)
			{
				bestLane = i;
				bestTotal = tmp;
			}
		}	
	}
	
	float EstimateSegmentTime(int pieceIdx, int laneIdx, CarPositions poses)
	{
		float totalTime = EstimatePieceLength(pieceIdx, laneIdx) / GetMaxSpeed(GetRadius(pieceIdx, laneIdx));
		foreach (var key in cars.Keys)
		{
			if (cars[key] == pieceIdx && GetCurrentLane(key, poses) == laneIdx)
			{
				System.Console.WriteLine("Next piece {0} lane {1} gets penalty!", pieceIdx, laneIdx);
				totalTime *= 1.1f;
			}
		}
		pieceIdx = GetNextIndex(pieceIdx);

		while (!init.data.race.track.pieces[pieceIdx].switch_)
		{
			totalTime += EstimatePieceLength(pieceIdx, laneIdx) / GetMaxSpeed(GetRadius(pieceIdx, laneIdx));
			pieceIdx = GetNextIndex(pieceIdx);
		}

		return totalTime;
	}

	float EstimateLaneLapTime(int laneIdx)
	{
		float totalTime = 0f;
		for (int i = 0; i < init.data.race.track.pieces.Length; i++)
		{
			totalTime += EstimatePieceLength(i, laneIdx) / GetMaxSpeed(GetRadius(i, laneIdx));
		}

		return totalTime;
	}

	float EstimateThrotle(int carIdx, int pieceIdx, CarPositions poses, float speed)
	{
		var currentInPieceDistance = poses.data[c].piecePosition.inPieceDistance;
		int currLane = GetCurrentLane(carIdx, poses);
		int currPiece = pieceIdx;
		float minSpeed = GetMaxSpeed(GetRadius(pieceIdx, currLane));
		float remainingDistance = EstimatePieceLength(pieceIdx, currLane) - currentInPieceDistance;
	
		for (int i = 0; i < init.data.race.track.pieces.Length - 1; i++)
			{
				currPiece = GetNextIndex(currPiece); 
				var mxSpeed = GetMaxSpeed(GetRadius(currPiece, bestLane));
				mxSpeed = EstimateMaxSpeedToPiece(mxSpeed, remainingDistance);
				
				if (mxSpeed < minSpeed)
					minSpeed = mxSpeed;
				
				remainingDistance += EstimatePieceLength(currPiece, bestLane);
			} 

		if (speed > minSpeed)
			return 0f;

		return GetMaxThrotle(minSpeed, speed);
	}

/*
	float EstimateThrotle(int carIdx, int pieceIdx, CarPositions poses, float speed)
	{
		float sp1 = GetMaxSpeed(GetRadius(carIdx, pieceIdx, poses));
		float sp2 = GetMaxSpeed(GetRadius(carIdx, GetNextIndex(pieceIdx), poses));
		if (speed > sp1 || speed > sp2)
			return 0f;
		return GetMaxThrotle((float)Math.Min(sp1, sp2));
	}
*/

	Bot(StreamReader reader, StreamWriter writer, Join join, JoinRace joinRace) {
		this.writer = writer;
		string line;

		if (joinRace != null)
			send(joinRace);
		else
			send(join);

		while((line = reader.ReadLine()) != null) {
			//System.Console.WriteLine(line);
			MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
			switch(msg.msgType) {
				case "carPositions":
			try
				{
					CarPositions poses = JsonConvert.DeserializeObject<CarPositions>(line);
					UpdateCarPositions(poses);
					c = GetCarIndex(poses);
					var currentInPieceDistance = poses.data[c].piecePosition.inPieceDistance;
					var curIdx = poses.data[c].piecePosition.pieceIndex;
					var radius = GetRadius(c, curIdx, poses);
					var lastTotalDist = totalDistance;

					if (lastIndex != curIdx)
					{
						totalDistance += EstimatePieceLength(lastIndex, GetCurrentLane(c, poses)); 
					}
					totalDistance += currentInPieceDistance - lastInPieceDistance; 
					var speed = totalDistance - lastTotalDist;

					var absangle = Math.Abs(poses.data[c].angle);
					var th = EstimateThrotle(c, curIdx, poses, speed);
					send(new Throttle(th));
					
					if (init.data.race.track.pieces[GetNextIndex(curIdx)].switch_)
						UpdateBestLane(GetNextIndex(curIdx), poses);
	
					var currLane = GetCurrentLane(c,poses);
					if (!switching && currLane != GetNextLane(c, poses))
					{
						switchSent = false;
						switching = true;
					}
					else if (switching && currLane == GetNextLane(c, poses))
					{
						switching = false;
					}

					if (!switchSent && !switching && gameStarted && currLane != bestLane)
					{
						System.Console.WriteLine("switching {0}", currLane > bestLane ? "Left" : "Right");
						if (currLane > bestLane)
							send(new SwitchLane("Left"));
						else
							send(new SwitchLane("Right"));
						
						//switchSent = true;
					}

						
					System.Console.WriteLine("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, b{9}. c{10}", 
											poses.gameTick, totalDistance, currentInPieceDistance, radius, 
											poses.data[c].angle, th, curIdx, GetCurrentLane(c, poses),
											GetNextLane(c, poses), bestLane, c);
					lastIndex = curIdx;
					lastInPieceDistance = currentInPieceDistance;
				}
				catch (Exception ex)
				{
					System.Console.WriteLine(ex.ToString());
					send(new Ping());
				}
					break;
				case "join":
					Console.WriteLine("Joined");
					send(new Ping());
					break;
				case "gameInit":
					init = JsonConvert.DeserializeObject<GameInit>(line);
					var bestTotal = 1e9f;
					for (int i = 0; i < init.data.race.track.lanes.Length; i++)
					{
						var tmp = EstimateLaneLapTime(i);
						if (tmp < bestTotal)
						{
							bestTotal = tmp;
							bestLane = i;
						}
					}
					
					System.Console.WriteLine("Best lane: {0}, laptime {1}", bestLane, bestTotal);
				//	Console.WriteLine("Race init {0}:{1}",init.data.race.track.id ,init.data.race.track.pieces.Length);
				//	foreach (var piece in init.data.race.track.pieces)
				//			System.Console.WriteLine(";;{0} {1} {2} {3}", piece.switch_, piece.length, piece.radius, piece.angle);
					send(new Ping());
					break;
				case "gameEnd":
					Console.WriteLine("Race ended");
					send(new Ping());
					break;
				case "lapFinished":
					var lapf = JsonConvert.DeserializeObject<LapFinished>(line);	
					Console.WriteLine("Lap {0} finished in {1} ms", lapf.data.lapTime.lap, lapf.data.lapTime.millis);
					send(new Ping());
					break;
				case "gameStart":
					Console.WriteLine("Race starts");
					gameStarted = true;
					send(new Ping());
					break;
				case "yourCar":
					car = JsonConvert.DeserializeObject<YourCar>(line);
					Console.WriteLine("Your car: {0}", car.data.color);
					send(new Ping());
					break;
				default:
					send(new Ping());
					break;
			}
		}
	}

	private void send(SendMsg msg) {
		writer.WriteLine(msg.ToJson());
	}
}

class CarData {
	public string name;
	public string color;
}

class LapFinished
{
	public string msgType;
	public LapData data;
}

class LapData
{
	public LapTime lapTime;
}

class LapTime
{
	public int lap;
	public int ticks;
	public int millis;
}


class YourCar
{
	public string msgType;
	public CarData data;
}

class Lane {
	public int startLaneIndex;
	public int endLaneIndex;
}

class PiecePosition {
	public int pieceIndex;
	public float inPieceDistance;
	public Lane lane;
	public int lap;
}

class CarId
{
	public string name;
	public string color;
}

class CarPosition {
	public CarId id;
	public float angle;
	public PiecePosition piecePosition;

	public CarPosition(CarId id, float angle, PiecePosition piecePosition)
	{
		this.id = id;
		this.angle = angle;
		this.piecePosition = piecePosition;
	}
}

class CarPositions {
	public string msgType;
	public CarPosition[] data;
	public string gameId;
	public int gameTick;
	
	public CarPositions(string msgType, CarPosition[] data, string gameId, int gameTick)
	{
		this.msgType = msgType;
		this.data = data;
		this.gameId = gameId;
		this.gameTick = gameTick;
	}
}

class Piece
{
	public float length;
	[JsonProperty("switch")]
	public bool switch_;
	public float angle;
	public float radius;
}

class LaneData
{
	public float distanceFromCenter;
	public int index;
}

class Track
{
	public string id;
	public string name;
	public Piece[] pieces;
	public LaneData[] lanes;
}

class Race
{
	public Track track;
}

class InitData
{
	public Race race;
}

class GameInit
{
	public string msgType;
	public InitData data;
}

class MsgWrapper {
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType, Object data) {
    	this.msgType = msgType;
    	this.data = data;
    }
}

abstract class SendMsg {
	public string ToJson() {
		return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
	}
	protected virtual Object MsgData() {
        return this;
    }

    protected abstract string MsgType();
}

class BotId
{
	public string name;
	public string key;
}

class SwitchLane : SendMsg
{
	public string direction;
	public SwitchLane(string dir)
	{
		direction = dir;
	}
	
	protected override Object MsgData()
	{
		return direction;
	}

	protected override string MsgType() {
		return "switchLane";
	}
}

class JoinRace : SendMsg
{
	public BotId botId;
	public string trackName;
	public string password;
	public int carCount;
	
	public JoinRace(string name, string key, string track)
	{
		botId = new BotId();
		botId.name = name;
		botId.key = key;
		trackName = track;
		password = "lazyaaa";
		carCount = 1;
	}
	
	protected override string MsgType() { 
		return "joinRace";
	}
}

class Join: SendMsg {
	public string name;
	public string key;

	public Join(string name, string key) {
		this.name = name;
		this.key = key;
	}

	protected override string MsgType() { 
		return "join";
	}
}

class Ping: SendMsg {
	protected override string MsgType() {
		return "ping";
	}
}

class Throttle: SendMsg {
	public double value;

	public Throttle(double value) {
		this.value = value;
	}

	protected override Object MsgData() {
		return this.value;
	}

	protected override string MsgType() {
		return "throttle";
	}
}
